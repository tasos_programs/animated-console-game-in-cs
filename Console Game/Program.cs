﻿using Console_Game;

// For now I'll consider main as the script.
/*
Animation player_animation = new();
player_animation.Parse_Animation("Animations\\player_greeting.txt");

var animation_name = "greeting";
var player_idle_animation = player_animation.Get_Animation(animation_name);
Player test = new("test", player_idle_animation[0], player_animation);
test.Speak("hello there my friend!", animation_name, 300);
*/
Play_Intro();

static void Play_Intro()
{
    Animation start_village = new();
    start_village.Parse_Animation("Animations\\start_village.txt");
    Character.Animate(start_village.Get_Animation("start_village"), 700);

    Animation player_animation = new();
    player_animation.Parse_Animation("Animations\\player_confused.txt");
    var player_confused = player_animation.Get_Animation("player_confused");
    Player player = new("", player_confused[0], player_animation);
    player.Speak("There once was a knight with amnesia.The only thing he could remember was his name. It was...","player_confused",500);
}