﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Console_Game
{
    internal class Character
    {
        protected string? Name { get; set; }
        protected string? Icon { get; set; }

        protected Animation Character_Animation = new();
        protected bool Skip_Animations { get; set; }

        protected Dictionary<string, string> Inventory = new();
        private void Say_Animated(string sentence, string animation_title, int sleep_time)
        {
            // Play the animation, then animate the sentence
            Play_Animation(animation_title, sleep_time);
            StringBuilder written = new();
            // Split the sentence by space
            string[] splitted = sentence.Split(' ');
            Animate_Text(ref written, ref splitted);
        }

        private void Play_Animation(string animation_title, int sleep_time)
        {
            // Play the animation
            var full_animation = Character_Animation.Get_Animation(animation_title);
            Animate(full_animation, sleep_time);
        }

        public static void Animate(List<string> full_animation, int sleep_time)
        {
            for (int i = 0; i < full_animation.Count; i++)
            {
                var animation = full_animation[i];
                Console.SetCursorPosition(0, 0);
                Console.WriteLine(animation);
                Thread.Sleep(sleep_time);
                // Put spaces to clear the screen. We don't want to do that on the last frame though!
                if (i == full_animation.Count - 1)
                    break;
                Clear_Console(animation);
            }
        }

        private static void Clear_Console(string animation)
        {
            Console.SetCursorPosition(0, 0);
            // Write spaces to clear the screen
            foreach (char c in animation)
                Console.Write(c == '\n' ? " \n" : ' ');
        }

        private static void Animate_Text(ref StringBuilder written, ref string[] splitted)
        {
            foreach (string word in splitted)
            {
                Animate_Letter(word, written);
                written.Append(word).Append(' ');
            }
        }

        private static void Animate_Letter(string word, StringBuilder written)
        {
            int i = 0;
            foreach (char letter in word)
            {
                // Get written size;
                Console.CursorLeft = written.Length + i;
                Console.Write(letter);
                i += 1;
                Thread.Sleep(100 + word.Length * 2);
            }
        }

        // ENTRY
        public void Speak(string sentence, string animation_name, int sleep_time)
        {
            if (!Skip_Animations)
            {
                Console.CursorVisible = false;
                Say_Animated(sentence, animation_name, sleep_time);
                Console.CursorVisible = true;
            }
            else
            {
                Console.WriteLine(sentence);
            }
        }

    }
    internal class Player : Character
    {
        public Player(string player_name, string player_icon, Animation player_animation, bool player_skip_animations = false)
        {
            Name = player_name;
            Icon = player_icon;
            Character_Animation = player_animation;
            Skip_Animations = player_skip_animations;
        }

        // Checkpoints go here vv
    }
}
