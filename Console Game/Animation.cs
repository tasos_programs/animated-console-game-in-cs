﻿using System.Text;

namespace Console_Game
{
    internal class Animation
    {
        private readonly Dictionary<string, List<string>> frames = new();

        private void New_Animation(string title)
        {
            frames[title] = new List<string>();
        }
        public void Add_Frame(string title, string frame)
        {
            frames[title].Add(frame);
        }

        public List<string> Get_Animation(string title)
        {
            return frames[title];
        }

        public string Get_Last_Frame(string title)
        {
            var animation = frames[title];
            return animation[^1];
        }

        public void Parse_Animation(string animation_file)
        {
            string[] contents;
            using (StreamReader animation_reader = new(animation_file))
            {
                contents = animation_reader.ReadToEnd().Split('\n');
            }

            string animation_title = contents[0].Split('\r')[0];
            New_Animation(animation_title);
            StringBuilder animation_builder = new();
            Loop_Contents(ref contents, animation_title, ref animation_builder);
        }

        private void Loop_Contents(ref string[] contents, string animation_title, ref StringBuilder animation_builder)
        {
            foreach (string frame in contents.Skip(1))
            {
                DecideFrameAction(animation_title, animation_builder, frame);
            }
        }

        private void DecideFrameAction(string animation_title, StringBuilder animation_builder, string frame)
        {
            if (frame == "end\r" || frame == "end")
            {
                frames[animation_title].Add(animation_builder.ToString());
                animation_builder.Clear();
            }
            else
            {
                animation_builder.AppendLine(frame);
            }
        }

        public IEnumerable<string> Play_Animation(string title)
        {
            foreach (string frame in frames[title])
            {
                yield return frame;
            }
        }
    }
}
